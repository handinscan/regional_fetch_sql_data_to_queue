package com.handinscan.regional.queue;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class RabbitMqMessageSender {
	private final Logger logger = LoggerFactory.getLogger(RabbitMqMessageSender.class);
	
	@Value("${handinscan.regional.queuename}")
	private String queueName;
	
	@Value("${handinscan.regional.username}")
	private String userName;
	
	@Value("${handinscan.regional.password}")
	private String password;
	
	@Value("${handinscan.regional.virtualhostname}")
	private String virtualHostName;
	
	@Value("${handinscan.regional.host}")
	private String host;
	
	@Value("${handinscan.regional.port}")
	private int port;
	
	@Value("${handinscan.regional.exchangename}")
	private String exchangeName;
	
	@Value("${handinscan.regional.routingkey}")
	private String routingKey;

	private Connection createConnection() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(userName);
		factory.setPassword(password);
		factory.setVirtualHost(virtualHostName);
		factory.setHost(host);
		factory.setPort(port);
		Connection connection = factory.newConnection();
		return connection;
	}

	public void send(String path) throws IOException, TimeoutException {
		Connection connection = createConnection();
		Channel channel = connection.createChannel();
		Map<String, Object> arguments = null;
		channel.queueDeclare(queueName, true, false, true, arguments);
		
		byte[] messageBodyBytes = path.getBytes();
		
		channel.basicPublish(exchangeName, routingKey, null, messageBodyBytes);
		logger.debug("............ successfully sent image path: " + path);
		
		channel.close();
		connection.close();
	}
}
