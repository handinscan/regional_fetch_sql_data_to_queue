package com.handinscan.regional.queue;

import static com.handinscan.regional.util.HandSideIdentifier.LEFT_DORSUM;
import static com.handinscan.regional.util.HandSideIdentifier.LEFT_PALM;
import static com.handinscan.regional.util.HandSideIdentifier.RIGHT_DORSUM;
import static com.handinscan.regional.util.HandSideIdentifier.RIGHT_PALM;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeoutException;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.handinscan.regional.dto.Measurement;
import com.handinscan.regional.filesystem.ImageFileHandler;
import com.handinscan.regional.util.HandSideIdentifier;

@Component
public class QueueWritter {
	private final Logger logger = LoggerFactory.getLogger(QueueWritter.class);
	
	private static final String URL_PREFIX = "https://";
	private static final String URL_POSTFIX = "-dot-skawa-handinscan.appspot.com/ui/landing/serveImage/?name=";
	
	@Autowired
	private RabbitMqMessageSender sender;
	
	@Autowired
	private ImageFileHandler fileHandler;
	
	@Value("${handinscan.regional.reportinginstance}")
	private String reportingInstanceWithoutLogin;
	
	private int imageCount = 0;
	
	public void pushMeasurementsToQueue(List<Measurement> measurements){
		if (isListBlank(measurements)) {
			return;
		}
		measurements.parallelStream().forEach(measurement -> {
			String leftDorsumEvaluationLink = measurement.getLeftDorsumEvaluation();
			pushSingleImageToQueue(leftDorsumEvaluationLink, LEFT_DORSUM, measurement.getId());
			
			String leftPalmEvaluationLink = measurement.getLeftPalmEvaluation();
			pushSingleImageToQueue(leftPalmEvaluationLink, LEFT_PALM, measurement.getId());
			
			String rightDorsumEvaluationLink = measurement.getRightDorsumEvaluation();
			pushSingleImageToQueue(rightDorsumEvaluationLink, RIGHT_DORSUM, measurement.getId());
			
			String rightPalmEvaluationLink = measurement.getRightPalmEvaluation();
			pushSingleImageToQueue(rightPalmEvaluationLink, RIGHT_PALM, measurement.getId());
		});
		logger.debug(".... exiting MeasurementReader.pushPageToQueue(...)");
	}

	private boolean isListBlank(List<?> measurements) {
		return measurements == null || measurements.size() == 0;
	}

	private void pushSingleImageToQueue(String imageLink, HandSideIdentifier handSideIdentifier, int id) {
		if (StringUtils.isBlank(imageLink)) {
			logger.debug("........ imageLink is blank, continuing with next one");
			return;
		}
		String imageUrl = createUrlFromImageLink(imageLink);
		logger.debug("........ imageUrl of image is: " + imageUrl);
		try {
			
			reportMemoryUsage("before reading image");
			
			URL url = new URL(imageUrl);
			BufferedImage image = ImageIO.read(url);
			
			imageCount++;
			reportMemoryUsage("after reading image, imageCount: " + imageCount);
			
			if (image == null) {
				logger.warn("........ can't push image to queue, it is null - should have been downloaded from :'" + imageUrl + "'");
				return;
			}
			String path = fileHandler.generateLocalImagePath(handSideIdentifier, id);
			fileHandler.writeImageToPath(image, path);
			
			reportMemoryUsage("after writting image to disc");
			
			String[] pathParts = path.split("/");
			int indexOfLastPart = pathParts.length - 1;
			String fileName = pathParts[indexOfLastPart];
			String dataToBeSent = fileName + ";" + path;
			sender.send(dataToBeSent);
		} catch (IOException e) {
			logger.error("IOException while fetching image from URL: " + imageUrl);
			e.printStackTrace();
		} catch (TimeoutException e) {
			logger.error("TimeoutException while sending image from URL: " + imageUrl);
			e.printStackTrace();
		}
	}
	
	private void reportMemoryUsage(String message) {
		Runtime runtime = Runtime.getRuntime();
		long freeMemory = runtime.freeMemory();
		long totalMemory = runtime.totalMemory();
		logger.debug(">> current memory usage " + message + ", totalMemory, freeMemory, totalMemory - freeMemory: " 
				+ totalMemory + ", " + freeMemory + ", " + (totalMemory - freeMemory));
		
	}

	private String createUrlFromImageLink(String imageLink) {
		if (imageLink.startsWith("http")) {
			return imageLink;
		}
		String reportingInstance = URL_PREFIX + reportingInstanceWithoutLogin + URL_POSTFIX;
		return reportingInstance + imageLink;
	}
}
