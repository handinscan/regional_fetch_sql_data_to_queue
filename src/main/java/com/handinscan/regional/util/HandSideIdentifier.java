package com.handinscan.regional.util;

public enum HandSideIdentifier {
	LEFT_DORSUM("left_dorsum"),
	LEFT_PALM("left_palm"),
	RIGHT_DORSUM("right_dorsum"),
	RIGHT_PALM("right_palm");
	
	private String name;
	
	private HandSideIdentifier(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
