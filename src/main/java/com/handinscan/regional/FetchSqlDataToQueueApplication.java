package com.handinscan.regional;

import java.sql.SQLException;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.handinscan.regional.dto.JdbcMeasurementDto;
import com.handinscan.regional.dto.Measurement;
import com.handinscan.regional.queue.QueueWritter;

@SpringBootApplication
public class FetchSqlDataToQueueApplication {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		ConfigurableApplicationContext context = SpringApplication.run(FetchSqlDataToQueueApplication.class, args);
		QueueWritter reader = context.getBean(QueueWritter.class);
		JdbcMeasurementDto dto = context.getBean(JdbcMeasurementDto.class);
		
		List<Measurement> measurement = dto.readByOptionsWithJdbcWhereClause(args[0]);
		reader.pushMeasurementsToQueue(measurement);
		
	}
}
