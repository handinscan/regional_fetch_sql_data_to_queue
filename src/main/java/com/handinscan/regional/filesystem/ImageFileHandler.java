package com.handinscan.regional.filesystem;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.handinscan.regional.util.HandSideIdentifier;


@Component
public class ImageFileHandler {
	private static final String SEPARATOR = "_";
	private static final String EXTENSION = ".png";
	
	@Value("${handinscan.regional.tempimagedirectory}")
	private String relativeImageDirectoryPath;
	
	public String generateLocalImagePath(HandSideIdentifier handSideIdentifier, int id) {
		String imageFolder = createImagesFolderPath();
		
		createImagesFolder(imageFolder);
		
		String path = createFilePath(imageFolder, handSideIdentifier, id);
		return path; 
	}

	private String createImagesFolderPath() {
		String currentDirectory = System.getProperty("user.dir");
		String imageFolder = currentDirectory + relativeImageDirectoryPath;
		return imageFolder;
	}
	
	private void createImagesFolder(String imageFolder) {
		File rootDirectory = new File(imageFolder);
		rootDirectory.mkdir();
	}

	private String createFilePath(String imageFolder, HandSideIdentifier handSideIdentifier, int id) {
		String path = imageFolder + String.valueOf(id) + SEPARATOR + handSideIdentifier.getName() + EXTENSION;
		return path;
	}
	
	public void writeImageToPath(BufferedImage image, String path) throws FileNotFoundException, IOException {
		File output = new File(path);
		output.createNewFile();
		ImageIO.write(image, "png", output);
	}
}
