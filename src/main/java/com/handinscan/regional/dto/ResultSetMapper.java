package com.handinscan.regional.dto;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class has been taken from http://www.codeproject.com/Tips/372152/Mapping-JDBC-ResultSet-to-Object-using-Annotations
 * The only change I made was a code cleanup
 * The purpose of this class is to map JDBC result sets to annotated classes without too much boilerplate
 * using reflection framework
 * 
 * @author balazs
 *
 * @param <T>
 */
public class ResultSetMapper<T> {
	private final Logger logger = LoggerFactory.getLogger(ResultSetMapper.class);
	
	@SuppressWarnings("unchecked")
	public List<T> mapRersultSetToObject(ResultSet resultSet, @SuppressWarnings("rawtypes") Class outputClass) {
		List<T> outputList = null;
		if (resultSet == null) {
			logger.warn("Result set is null for output class: " + outputClass);
			return null;
		}
		if (!outputClass.isAnnotationPresent(Entity.class)) {
			logger.warn("No @Entity annotation found in class: " + outputClass);
		}
		try {
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			Field[] fields = outputClass.getDeclaredFields();
			while (resultSet.next()) {
				T bean = (T) outputClass.newInstance();
				for (int _iterator = 0; _iterator < resultSetMetaData.getColumnCount(); _iterator++) {
					String columnName = resultSetMetaData.getColumnName(_iterator + 1);
					Object columnValue = resultSet.getObject(_iterator + 1);
					for (Field field : fields) {
						if (field.isAnnotationPresent(Column.class)) {
							Column column = field.getAnnotation(Column.class);
							if (column.name().equalsIgnoreCase(columnName) && columnValue != null) {
								BeanUtils.setProperty(bean, field.getName(), columnValue);
								break;
							}
						}
					}
				}
				if (outputList == null) {
					outputList = new ArrayList<T>();
				}
				outputList.add(bean);
			}
		} catch (ReflectiveOperationException e) {
			logger.error("Error using reflection framework: " + e.getMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("SQLException: " + e.getMessage());
			e.printStackTrace();
		}
		return outputList;
	}
}