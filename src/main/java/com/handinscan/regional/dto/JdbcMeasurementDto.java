package com.handinscan.regional.dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public class JdbcMeasurementDto {
	private static final String QUERY_SELECT_FROM = "   SELECT measurement.id, measurement.scannerId, measurement.leftDorsumCoverage, measurement.leftPalmCoverage, measurement.rightDorsumCoverage, measurement.rightPalmCoverage, measurement.leftDorsumEvaluation, measurement.leftPalmEvaluation, measurement.rightDorsumEvaluation, measurement.rightPalmEvaluation, measurement.leftDorsumResult, measurement.leftPalmResult, measurement.rightDorsumResult, measurement.rightPalmResult, measurement.leftHandRing, measurement.rightHandRing "
													+ " FROM Measurement measurement";
	
	private static final String QUERY_SELECT_FROM_WHERE = QUERY_SELECT_FROM
			+ " WHERE measurement.id BETWEEN ? AND ?"
			+ "		AND measurement.scannerId = ?";

	@Value("${spring.datasource.driver-class-name}")
	private String driverClassName;
	
	@Value("${spring.datasource.url}")
	private String dataSourceUrl;
	
	@Value("${spring.datasource.username}")
	private String userName;
	
	@Value("${spring.datasource.password}")
	private String password;

	public List<Measurement> readByOptions(Integer fromId, Integer toId, String scannerId) throws ClassNotFoundException, SQLException {
		Connection connection = createConnection();
		PreparedStatement statement = connection.prepareStatement(QUERY_SELECT_FROM_WHERE);
		statement.setInt(1, fromId);
		statement.setInt(2, toId);
		statement.setString(3, scannerId);
		List<Measurement> measurements = executeMeasurementQuery(statement);
		return measurements;
	}

	private Connection createConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driverClassName);
		Connection connection = DriverManager.getConnection(dataSourceUrl, userName, password);
		return connection;
	}
	
	private List<Measurement> executeMeasurementQuery(PreparedStatement statement) throws SQLException {
		ResultSet resultSet = null;
		resultSet = statement.executeQuery();
		ResultSetMapper<Measurement> resultSetMapper = new ResultSetMapper<Measurement>();
		List<Measurement> pojoList = resultSetMapper.mapRersultSetToObject(resultSet, Measurement.class);
		return pojoList;
	}
	
	public List<Measurement> readByOptionsWithJdbcWhereClause(String where) throws ClassNotFoundException, SQLException {
		Connection connection = createConnection();
		String query = QUERY_SELECT_FROM + where;
		PreparedStatement statement = connection.prepareStatement(query);
		List<Measurement> measurements = executeMeasurementQuery(statement);
		return measurements;
	}
}
