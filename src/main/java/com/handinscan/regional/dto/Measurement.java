package com.handinscan.regional.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.search.annotations.Field;

@Entity
public class Measurement {
	  @Id
	  @Column(name="id")
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private int id;
	  
	  @Field(name = "scannerID")
	  @Column(name="scannerID")
	  @NotNull
	  private String scannerId;
	  
	  @Field
	  @Column(name="leftDorsumCoverage")
	  @NotNull
	  private float leftDorsumCoverage;
	  
	  @Field
	  @Column(name="leftDorsumEvaluation")
	  @NotNull
	  private String leftDorsumEvaluation;
	  
	  @Field
	  @Column(name="leftDorsumResult")
	  @NotNull
	  private boolean leftDorsumResult;
	  
	  @Field
	  @Column(name="leftHandRing")
	  @NotNull
	  private boolean leftHandRing;
	  
	  @Field
	  @Column(name="leftPalmCoverage")
	  @NotNull
	  private float leftPalmCoverage;	
	  
	  @Field
	  @Column(name="leftPalmEvaluation")
	  @NotNull
	  private String leftPalmEvaluation;
	  
	  @Field
	  @Column(name="leftPalmResult")
	  @NotNull
	  private boolean leftPalmResult;	
	  
	  @Field
	  @Column(name="rightDorsumCoverage")
	  @NotNull
	  private float rightDorsumCoverage;
	  
	  @Field
	  @Column(name="rightDorsumEvaluation")
	  @NotNull
	  private String rightDorsumEvaluation;
	  
	  @Field
	  @Column(name="rightDorsumResult")
	  @NotNull
	  private boolean rightDorsumResult;

	  @Field
	  @Column(name="rightHandRing")
	  @NotNull
	  private boolean rightHandRing;
	  
	  @Field
	  @Column(name="rightPalmCoverage")
	  @NotNull
	  private float rightPalmCoverage;	
	  
	  @Field
	  @Column(name="rightPalmEvaluation")
	  @NotNull
	  private String rightPalmEvaluation;
	  
	  @Field
	  @Column(name="rightPalmResult")
	  @NotNull
	  private boolean rightPalmResult;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getScannerId() {
		return scannerId;
	}

	public void setScannerId(String scannerId) {
		this.scannerId = scannerId;
	}

	public float getLeftDorsumCoverage() {
		return leftDorsumCoverage;
	}

	public void setLeftDorsumCoverage(float leftDorsumCoverage) {
		this.leftDorsumCoverage = leftDorsumCoverage;
	}

	public String getLeftDorsumEvaluation() {
		return leftDorsumEvaluation;
	}

	public void setLeftDorsumEvaluation(String leftDorsumEvaluation) {
		this.leftDorsumEvaluation = leftDorsumEvaluation;
	}

	public boolean isLeftDorsumResult() {
		return leftDorsumResult;
	}

	public void setLeftDorsumResult(boolean leftDorsumResult) {
		this.leftDorsumResult = leftDorsumResult;
	}

	public boolean isLeftHandRing() {
		return leftHandRing;
	}

	public void setLeftHandRing(boolean leftHandRing) {
		this.leftHandRing = leftHandRing;
	}

	public float getLeftPalmCoverage() {
		return leftPalmCoverage;
	}

	public void setLeftPalmCoverage(float leftPalmCoverage) {
		this.leftPalmCoverage = leftPalmCoverage;
	}

	public String getLeftPalmEvaluation() {
		return leftPalmEvaluation;
	}

	public void setLeftPalmEvaluation(String leftPalmEvaluation) {
		this.leftPalmEvaluation = leftPalmEvaluation;
	}

	public boolean isLeftPalmResult() {
		return leftPalmResult;
	}

	public void setLeftPalmResult(boolean leftPalmResult) {
		this.leftPalmResult = leftPalmResult;
	}

	public float getRightDorsumCoverage() {
		return rightDorsumCoverage;
	}

	public void setRightDorsumCoverage(float rightDorsumCoverage) {
		this.rightDorsumCoverage = rightDorsumCoverage;
	}

	public String getRightDorsumEvaluation() {
		return rightDorsumEvaluation;
	}

	public void setRightDorsumEvaluation(String rightDorsumEvaluation) {
		this.rightDorsumEvaluation = rightDorsumEvaluation;
	}

	public boolean isRightDorsumResult() {
		return rightDorsumResult;
	}

	public void setRightDorsumResult(boolean rightDorsumResult) {
		this.rightDorsumResult = rightDorsumResult;
	}

	public boolean isRightHandRing() {
		return rightHandRing;
	}

	public void setRightHandRing(boolean rightHandRing) {
		this.rightHandRing = rightHandRing;
	}

	public float getRightPalmCoverage() {
		return rightPalmCoverage;
	}

	public void setRightPalmCoverage(float rightPalmCoverage) {
		this.rightPalmCoverage = rightPalmCoverage;
	}

	public String getRightPalmEvaluation() {
		return rightPalmEvaluation;
	}

	public void setRightPalmEvaluation(String rightPalmEvaluation) {
		this.rightPalmEvaluation = rightPalmEvaluation;
	}

	public boolean isRightPalmResult() {
		return rightPalmResult;
	}

	public void setRightPalmResult(boolean rightPalmResult) {
		this.rightPalmResult = rightPalmResult;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Measurement [id=");
		builder.append(id);
		builder.append(", leftDorsumCoverage=");
		builder.append(leftDorsumCoverage);
		builder.append(", leftDorsumEvaluation=");
		builder.append(leftDorsumEvaluation);
		builder.append(", leftDorsumResult=");
		builder.append(leftDorsumResult);
		builder.append(", leftHandRing=");
		builder.append(leftHandRing);
		builder.append(", leftPalmCoverage=");
		builder.append(leftPalmCoverage);
		builder.append(", leftPalmEvaluation=");
		builder.append(leftPalmEvaluation);
		builder.append(", leftPalmResult=");
		builder.append(leftPalmResult);
		builder.append(", rightDorsumCoverage=");
		builder.append(rightDorsumCoverage);
		builder.append(", rightDorsumEvaluation=");
		builder.append(rightDorsumEvaluation);
		builder.append(", rightDorsumResult=");
		builder.append(rightDorsumResult);
		builder.append(", rightHandRing=");
		builder.append(rightHandRing);
		builder.append(", rightPalmCoverage=");
		builder.append(rightPalmCoverage);
		builder.append(", rightPalmEvaluation=");
		builder.append(rightPalmEvaluation);
		builder.append(", rightPalmResult=");
		builder.append(rightPalmResult);
		builder.append("]");
		return builder.toString();
	}	

}
